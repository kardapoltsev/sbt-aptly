/*
 * Copyright 2020 Alexey Kardapoltsev
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.github.kardapoltsev.sbtaptly

import java.io.ByteArrayInputStream
import java.nio.file.Files

import com.typesafe.sbt.packager.debian.DebianPlugin
import sbt.Keys._
import sbt._
import scalaj.http.{Http, HttpOptions, HttpResponse, MultiPart}

object SbtAptlyPlugin extends AutoPlugin {

  override def requires: Plugins = super.requires && DebianPlugin
  import DebianPlugin.autoImport._

  /**
    * Defines all settings/tasks that get automatically imported,
    * when the plugin is enabled
    */
  object autoImport {
    lazy val aptlyUrl: SettingKey[String] =
      settingKey[String]("Aptly server api http url")
    lazy val aptlyRepoName: SettingKey[String] =
      settingKey[String]("The name of the debian repo in the aptly server ")
    lazy val aptlyPrefix: SettingKey[String] =
      settingKey[String]("The name of the repo prefix in the aptly server ")
    lazy val aptlyDistribution: SettingKey[String] =
      settingKey[String]("The OS distribution name of the aptly repo")
    lazy val aptlyDebianPackage: TaskKey[File] =
      taskKey[java.io.File]("The file for the debian package")
    // returns published filename
    lazy val aptlyPublish: TaskKey[String] = TaskKey[String]("aptly-publish", "Publish debian package to aptly repo")
  }

  import autoImport._

  override lazy val projectSettings: Seq[Setting[_]] = Seq(
    aptlyDebianPackage := (packageBin in Debian).value,
    aptlyPublish := {
      val aUrl          = aptlyUrl.value
      val aRepoName     = aptlyRepoName.value
      val aPrefix       = aptlyPrefix.value
      val aDistribution = aptlyDistribution.value
      val debFile       = aptlyDebianPackage.value
      val log           = streams.value.log
      val uploadDir     = "temp_upload_dir_" + java.util.UUID.randomUUID.toString

      /**
        * Step 1: upload the debian package to temp location
        * http://www.aptly.info/doc/api/files/
        */
      def uploadDebianFile(): Unit = {
        val start     = System.currentTimeMillis()
        val uploadUrl = s"$aUrl/files/$uploadDir"
        log.info(s"uploading ${debFile.getName} to $uploadUrl")
        val bytes = Files.readAllBytes(debFile.toPath)
        val response = Http(uploadUrl)
          .postMulti(
            MultiPart(
              name = debFile.getName,
              filename = debFile.getName,
              mime = "",
              data = new ByteArrayInputStream(bytes),
              numBytes = bytes.length,
              writtenBytes => {
                val p       = (writtenBytes.toDouble * 100) / bytes.length
                val elapsed = (System.currentTimeMillis() - start) / 1000
                print(f"\rpublishing deb $p%3.2f%% ($elapsed seconds)")
              },
            )
          )
          .option(HttpOptions.readTimeout(0))
          .sendBufferSize(64 * 1024)
          .asString
        checkHttpResponse("UploadFile", response)
      }

      /**
        * Step 2: Copy the debian package from temp location to the repo
        * http://www.aptly.info/doc/api/repos/
        * POST /api/repos/:name/file/:dir/:file
        */
      def moveToRepo(): Unit = {
        log.debug("starting MoveToRepo stage")
        val debianPackageFileName = debFile.getName
        val addToRepoUrl          = s"$aUrl/repos/$aRepoName/file/$uploadDir/$debianPackageFileName"
        val response              = Http(addToRepoUrl).method("POST").asString
        checkHttpResponse("MoveToRepo", response)
      }

      /**
        * Step: 3 repo publish update
        * http://www.aptly.info/doc/api/publish/
        * PUT /api/publish/:prefix/:distribution
        *
        * This assumes that the repo has been published earlier and is not a new one
        */
      def publishUpdate(): Unit = {
        val publishUrl = s"$aUrl/publish/:$aPrefix/$aDistribution"
        log.info(s"starting publishing of $publishUrl")
        val response =
          Http(publishUrl)
            .put("""{"ForceOverwrite": true, "Signing": {"Skip": true}}""")
            .header("content-type", "application/json")
            .asString
        checkHttpResponse("PublishRepo", response)

      }

      uploadDebianFile()
      moveToRepo()
      publishUpdate()
      debFile.getName
    },
  )

  private def checkHttpResponse(stage: String, response: HttpResponse[String]): Unit = {
    if (!response.isSuccess) {
      throw new Exception(s"step $stage failed: ${response.body}")
    }
  }

}
