lazy val root = (project in file("."))
  .settings(
    name := "test-publish-name",
    version := "0.0.1",
    scalaVersion := "2.12.10",
    maintainer := "aptly@example.com",
    packageSummary := "Test aptly publish",
    packageDescription := "description",
    aptlyUrl := "http://repo.localhost/api",
    aptlyPrefix := ".",
    aptlyRepoName := "wheezy",
    aptlyDistribution := "dev",
  )
  .enablePlugins(JDebPackaging)
  .enablePlugins(SbtAptlyPlugin)
