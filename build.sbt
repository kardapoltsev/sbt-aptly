Global / onChangedBuildSource := ReloadOnSourceChanges

name := "sbt-aptly"
organization := "com.github.kardapoltsev"
organizationName := "Alexey Kardapoltsev"
organizationHomepage := Some(url("https://github.com/kardapoltsev"))
scalaVersion := "2.12.10"
crossScalaVersions := Seq(scalaVersion.value)
Global / onChangedBuildSource := ReloadOnSourceChanges

enablePlugins(SbtPlugin)
addSbtPlugin("com.typesafe.sbt" % "sbt-native-packager" % "1.7.5")

scriptedLaunchOpts ++= Seq("-Xmx1024M", "-Dplugin.version=" + version.value)
scriptedBufferLog := false

// scalafix configs
addCompilerPlugin(scalafixSemanticdb)
scalacOptions ++= Seq(
  "-Yrangepos",
  "-Ywarn-unused-import",
)
addCommandAlias("fixall", "all compile:scalafix test:scalafix")
addCommandAlias("checkall", "; compile:scalafix --check ; test:scalafix --check")

//sbt-release configuration
releasePublishArtifactsAction := PgpKeys.publishSigned.value
releaseCrossBuild := true

//publish configuration
publishMavenStyle := true
publishTo := Some(
  if (isSnapshot.value)
    Opts.resolver.sonatypeSnapshots
  else
    Opts.resolver.sonatypeStaging
)
homepage := Some(url("https://github.com/kardapoltsev/sbtaptly"))
scmInfo := Some(
  ScmInfo(
    url("https://github.com/kardapoltsev/sbtaptly"),
    "scm:git@github.com:kardapoltsev/sbtaptly.git",
  )
)
developers := List(
  Developer(
    id = "kardapoltsev",
    name = "Alexey Kardapoltsev",
    email = "alexey.kardapoltsev@gmail.com",
    url = url("https://github.com/kardapoltsev"),
  )
)

libraryDependencies ++= Seq(
  "org.scalaj"    %% "scalaj-http" % "2.4.2",
  "org.scalatest" %% "scalatest"   % "3.2.2" % "test",
)

// sbt-header configuration
licenses := Seq(("Apache-2.0", new URL("https://www.apache.org/licenses/LICENSE-2.0.txt")))
startYear := Some(2020)
enablePlugins(AutomateHeaderPlugin)
